[33mcommit 7fe9d259c38f985382101b1209705a579e4082bc[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: Moisescu, Vlad (UG - Computer Science) <vm00353@surrey.ac.uk>
Date:   Fri Mar 6 15:47:19 2020 +0000

    Discuss concerns about Mars' climate for Mummy

[33mcommit c4d0a3ae8d5ef31ca679fffcce22ca17aac10e21[m
Author: Moisescu, Vlad (UG - Computer Science) <vm00353@surrey.ac.uk>
Date:   Fri Mar 6 15:44:01 2020 +0000

    Add concerns about effects of Mars' moons on Wolfman

[33mcommit f6b4de8b91c0fd12934690f28bf37a05580fb8b5[m
Author: Moisescu, Vlad (UG - Computer Science) <vm00353@surrey.ac.uk>
Date:   Fri Mar 6 15:37:30 2020 +0000

    Start notes on Mars as a base
